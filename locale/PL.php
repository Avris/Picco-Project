<?php
return [
    'documentation' => 'Dokumentacja',
    'documentation.language' => 'Dokumentacja jest dostępna tylko po angielsku',
    'item' => 'Element',
    'items' => 'Elementy',
    'items.demo' => 'Elementy nie robią nic konkretnego, to tylko demo działania routingu',
    'items.empty' => 'Nie znaleziono żadnych elementów',
    'items.generate' => 'Generuj nowe',
    'items.generate.label' => 'Ile elementów wygenerować?',
    'items.generate.success' => 'Wygenerowano %number% elementów',
    'item.value' => 'Wartość',
    'item.delete.confirm' => 'Czy na pewno chcesz usunąć %name%?',
    'item.delete.success' => '%name% został usunęty',
    'back' => 'Wróć',
    'delete' => 'Usuń',
    'size' => 'Rozmiar kodu',
];