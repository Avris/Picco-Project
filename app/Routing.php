<?php
namespace App;

class Routing
{
    public static function get()
    {
        return [
            'home' => '/',
            'size' => '/size',
            'itemList' => '/item/list',
            'itemGenerate' => '/item/generate',
            'itemShow' => '/item/(\d+)/show',
            'itemDelete' => '/item/(\d+)/delete/?(.*)',
            'changeLocale' => '/locale/([A-Z]{2})'
        ];
    }
}
