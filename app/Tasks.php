<?php
namespace App;

use Picco\Container;

class Tasks
{
    public function help()
    {
        foreach((new \ReflectionClass(static::class))->getMethods(\ReflectionMethod::IS_PUBLIC)as$n)
            echo sprintf(" - %s (%s)\n",$n->name,join(',',array_map(function($a){return$a->name;},$n->getParameters())));
    }

    public function size(Container $c)
    {
        list($files, $sum) = array_values($c->sizeChecker->check());
        foreach ($files as $name => $size) {
            echo "$name\t$size B\n";
        }
        echo "\t\t\t = $sum B\n";
    }

    public function fixtures(Container $c)
    {
        $c->db->nuke();
        $c->itemGenerator->generate(24);

        echo "Fixtures loaded\n";
    }

    public function test(Container $c, $x=null)
    {
        echo $c->foo.$x."\n";
    }
}
