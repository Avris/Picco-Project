<?php
namespace App\Service;

class SizeChecker
{
    public function check()
    {
        $files = [];
        $sum = 0;

        $dir = D . 'vendor/avris/picco/';
        foreach([-1 => $dir . 'picco'] + glob($dir.'src/*') as $f) {
            $sum += $files[basename($f)] = strlen(join('',file($f)));
        }

        return ['files' => $files, 'sum' => $sum];
    }
}