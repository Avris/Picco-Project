<?php
namespace App;

use Picco\Container;

class Controllers
{
    public function home(Container $c)
    {
        if ($c->locale !== 'EN') {
            $this->flash($c->translations['documentation.language']);
        }

        return [
            'readme' => $c->readme,
        ];
    }

    public function size(Container $c)
    {
        $cache = $c->cache;
        $cached = $cache('size.json', function ($c) { return json_encode($c->sizeChecker->check()); });

        return [
            'sizes' => json_decode($cached, true),
        ];
    }

    public function itemList(Container $c)
    {
        return ['items' => $c->db->findAll('item')];
    }

    public function itemGenerate(Container $c)
    {
        if ($this->isPost()) {
            $n = (int)@$_POST['number'];
            if ($n < 1 || $n > 100) {
                throw new \InvalidArgumentException('Form invalid');
            }

            $c->itemGenerator->generate($n);
            $this->flash(strtr($c->translations['items.generate.success'], ['%number%' => $n]));
            $this->redirect($c, 'itemList');
        }
    }

    public function itemShow(Container $c, $id)
    {
        $item = $c->db->load('item', $id);
        if (!$item->id) { throw new \Exception("Item $id not found", 404); }

        return [
            'item' => $item,
            'foo' => $c->foo,
        ];
    }

    public function itemDelete(Container $c, $id)
    {
        $item = $c->db->load('item', $id);
        if (!$item->id) { throw new \Exception("Item $id not found", 404); }

        if ($this->isPost()) {
            $c->db->trash($item);
            $this->flash(strtr($c->translations['item.delete.success'], ['%name%' => $item->name]));
            $this->redirect($c, 'itemList');
        }

        return ['item' => $item];
    }

    public function changeLocale(Container $c, $locale)
    {
        if (!in_array($locale, $c->locales)) { throw new \Exception('Invalid locale', 404); }
        $_SESSION['locale'] = $locale;
        $this->redirect($c, 'home', @$_SERVER['HTTP_REFERER']);
    }

    public function error(Container $c, \Exception $e)
    {
        $code = $e->getCode();
        if ($code < 200 || $code > 599) {
            $code = 500;
        }

        return [
            'code' => $code,
            'message' => $code == 404 ? 'Not found' : 'Internal server error',
        ];
    }

    protected function redirect(Container $c, $route, $direct = null)
    {
        header('location: ' . ($direct ?: $c->router->get($route)));exit;
    }

    protected function flash($message)
    {
        $_SESSION['flash'][] = $message;
    }

    protected function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }
}
