<?php
return [
    'documentation' => 'Dokumentation',
    'documentation.language' => 'Die Dokumentation ist nur auf Englisch verfügbar',
    'item' => 'Objekt',
    'items' => 'Objekte',
    'items.demo' => 'Die Objekte tun gar nix, das ist nur die Vorstellung von routing',
    'items.empty' => 'NKeine Objekte gefunden.',
    'items.generate' => 'Neue generieren',
    'items.generate.label' => 'Wie viele Objekte generieren?',
    'items.generate.success' => '%number% Objekte generiert',
    'item.value' => 'Wert',
    'item.delete.confirm' => 'Bist du sicher, dass du %name% löschen möchtest?',
    'item.delete.success' => '%name% gelöscht',
    'back' => 'Zurück',
    'delete' => 'Löschen',
    'size' => 'Codegröße',
];