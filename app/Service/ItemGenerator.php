<?php
namespace App\Service;

class ItemGenerator
{
    /** @var \R */
    protected $db;

    public function __construct(\R $db)
    {
        $this->db = $db;
    }

    public function generate($n = 10)
    {
        for ($i = 0; $i < $n; $i++) {
            $item = $this->db->dispense('item');
            $item->name = 'Item'.strtoupper(substr(md5(uniqid()),0,5));
            $item->value = md5(uniqid());
            $this->db->store($item);
        }
    }
}