<?php
return [
    'documentation' => 'Documentation',
    'documentation.language' => 'Documentation is only available in English',
    'item' => 'Item',
    'items' => 'Items',
    'items.empty' => 'No items found in the database.',
    'items.demo' => 'The items don\'t do anything, it\'s just a demo of routing',
    'items.generate' => 'Generate new',
    'items.generate.label' => 'How many items should be generated?',
    'items.generate.success' => '%number% items generated',
    'item.value' => 'Value',
    'item.delete.confirm' => 'Are you sure you want to delete %name%?',
    'item.delete.success' => '%name% has been deleted',
    'back' => 'Back',
    'delete' => 'Delete',
    'size' => 'Code size',
];