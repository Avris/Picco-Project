<?php
namespace App;

use App\Service\ItemGenerator;
use App\Service\SizeChecker;
use Picco\Container;
use Picco\Dispatcher;
use RedBeanPHP\OODBBean;

class Services
{
    public static function get(Container $c)
    {
        $c->foo = 'bar';

        $c->parameters = require D.'parameters.php';

        self::configureListeners($c->dispatcher);

        $c->db = function($c) {
            define('REDBEAN_MODEL_PREFIX','\\App\\Model\\');
            $p = $c->parameters['db'];
            $db = new \R;
            $db->setup($p['dsn'], $p['user'], $p['pass']);
            if (!E) {$db->freeze();}
            return $db;
        };

        $c->itemGenerator = function(Container $c) {
            return new ItemGenerator($c->db);
        };

        $c->locales = ['EN','DE','PL'];
        $c->fallbackLocale = 'EN';


        $c->locale = function (Container $c) {
            $locale = @$_SESSION['locale'] ?: $c->fallbackLocale;
            if (!in_array($locale, $c->locales)) { throw new \Exception('Invalid locale', 404); }
            return $locale;
        };

        $c->translations = function(Container $c) {
            return require D . 'locale/' . $c->locale . '.php';
        };

        $c->cache = function(Container $c) {
            return function($filename, $callback) use ($c) {
                if (E) {
                    return $callback($c);
                }

                $path = D . 'run/' . $filename;
                if (file_exists($path)) {
                    return file_get_contents($path);
                }

                @mkdir(dirname($path), 0777, true);

                $content = $callback($c);
                file_put_contents($path, $content);

                return $content;
            };
        };

        $c->sizeChecker = function(Container $c) { return new SizeChecker(); };

        $c->readme = function(Container $c) {
            /** @var \Closure $cache */
            $cache = $c->cache;
            return $cache('readme.html', function() {
                $md = new \Parsedown();
                $html = $md->parse(file_get_contents(D.'vendor/avris/picco/README.md'));
                return strtr($html, [
                    '--' => '–',
                    '<a ' => '<a target="_blank" ',
                ]);
            });
        };
    }

    private static function configureListeners(Dispatcher $dispatcher)
    {
        $dispatcher->request = function(Container $c, $action, $parameters) {
            if (isset($_GET['foo'])) {
                header('content-type: application/json');
                return json_encode(['foo' => $c->foo]);
            }
        };

        $dispatcher->response = function(Container $c, \stdClass $vars) {
            $vars->locales = $c->locales;
            $vars->currentLocale = $c->locale;
            $vars->t = $c->translations;
        };

        $dispatcher->response = function(Container $c, \stdClass $vars) {
            if (isset($_GET['format']) && $_GET['format'] == 'json') {
                header('content-type: application/json');
                array_walk_recursive($vars, function(&$el) {
                    if ($el instanceof OODBBean) {
                        $el = $el->box();
                    }
                });
                return json_encode($vars);
            }
        };

        $dispatcher->error = function(Container $c, \Exception $e) {
            @file_put_contents(
                D . '/run/error.log',
                date('Y-m-d H:i:s') . "\n" . $e->getMessage() . "\n" . $e->getTraceAsString() . "\n\n",
                FILE_APPEND
            );
        };
    }
}
