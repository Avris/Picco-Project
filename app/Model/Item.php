<?php
namespace App\Model;

class Item extends \RedBean_SimpleModel implements \JsonSerializable
{
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
        ];
    }
}